package gocloc

import (
	"archive/zip"
	"bytes"
	"io"
	"os"
	"path/filepath"
)

func IsZip(zipPath string) bool {
	f, err := os.Open(zipPath)
	if err != nil {
		return false
	}
	defer f.Close()

	buf := make([]byte, 4)
	if n, err := f.Read(buf); err != nil || n < 4 {
		return false
	}

	return bytes.Equal(buf, []byte("PK\x03\x04"))
}


func Unzip(archive, target string) error {
	reader, err := zip.OpenReader(archive)
	if err != nil {
		return err
	}

	if err := os.MkdirAll(target, 0755); err != nil {
		return err
	}

	for _, file := range reader.File {
		path := filepath.Join(target, file.Name)
		if file.FileInfo().IsDir() {
			os.MkdirAll(path, file.Mode())
			continue
		}

		fileReader, err := file.Open()
		if err != nil {
			return err
		}
		defer fileReader.Close()

		targetFile, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, file.Mode())
		if err != nil {
			return err
		}
		defer targetFile.Close()

		if _, err := io.Copy(targetFile, fileReader); err != nil {
			return err
		}
	}

	return nil
}


func IsDir(path string) bool {
	s, err := os.Stat(path)

	if err != nil {

		return false

	}

	return s.IsDir()

}
//func main() {
//	zipPath := "/home/xjj/Project/Java/solutiondemo.zip"
//	fmt.Println(path.Dir(zipPath))
//	result := IsZip(zipPath)
//	fmt.Println(result)
//	Unzip("/home/xjj/Project/Java/solutiondemo.zip", path.Dir(zipPath))
//}